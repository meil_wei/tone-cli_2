<div align="center">
<h1>tone command line</h1>
</div>

English | [简体中文](README-zh_CN.md)

## Install

```bash
make install
```

## Usage

* Step 1: Get the integrated test suites

```bash
$ tone list
unixbench                                          performance
```

* Step 2: Fetch the repository of test suite

```bash
$ tone fetch unixbench
```

* Step 3: Install the test suite

```bash
$ tone install unixbench
```

* Step 4: Run test suite

```bash
$ tone run unixbench
```