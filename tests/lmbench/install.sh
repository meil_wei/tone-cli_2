DEP_PKG_LIST="patch make gcc rpcbind parted"
GIT_URL="https://gitee.com/rtoax/lmbench.git"

build() {
    rpcs=$(rpm -ql glibc-headers | grep 'rpc/rpc.h' | wc -l)
    if [[ $rpcs -lt 1 ]];then
        DEP_PKG_LIST+=" libtirpc-devel" install_pkg
        patch -p1 <$TONE_BM_SUITE_DIR/rpc_patch/fix_rpc_error.patch
    fi
    make
}

install() {
	cp -rf * $TONE_BM_RUN_DIR/
}