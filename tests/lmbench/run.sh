#!/bin/bash
. $TONE_ROOT/lib/disk.sh
. $TONE_ROOT/lib/testinfo.sh

init_disk() {
    nr_disk=1
    fs=${fs:-ext4}
    disk_type=${disk_type:-disk}
    setup_disk_fs "$disk_type" "$nr_disk" "mounted"
}


run() {
    add_testinfo_cpu_model
    if [[ -n $fs ]];then init_disk;fi
    fsdir=$(get_mount_points)
    fsdir=${fsdir:-/tmp}

    C=$TONE_BM_RUN_DIR/CONFIG.$(hostname)

    if [ $(ls "$TONE_BM_RUN_DIR"/bin | wc -l) == 1 ];then
        OS=`ls "$TONE_BM_RUN_DIR"/bin`
	bindir="./bin/$OS"
    else
        OS=''	
        bindir="./bin"
    fi

    export PATH=$TONE_BM_RUN_DIR/bin/$OS:$PATH
    echo DISKS=\"$DISKS\" > $C
    echo DISK_DESC=\"$DISK_DESC\" >> $C
    echo OUTPUT="/dev/tty" >> $C
    echo ENOUGH=`./bin/$OS/enough` >> $C
    echo FASTMEM="NO" >> $C
    echo FILE="$fsdir/lmbench/XXX" >> $C
    echo FSDIR="$fsdir/lmbench" >> $C
    echo INFO=`./scripts/info` >> $C
    echo LOOP_O=`./bin/$OS/loop_o` >> $C
    echo MAIL="NO" >> $C

    TMP=`grep 'MemTotal:' /proc/meminfo | awk '{print $2}'`
    echo TOTAL_MEM=`echo $TMP / 1024 | bc 2>/dev/null` >> $C
    MB=$(( $(size_cn_bytes $memory) / 2 / 1024 / 1024 ))
    #reduce test time
    [[ $MB -ge 10240 ]] && MB=10240

    echo MB=${MB:-${TOTAL_MEM}} >> $C
    echo LINE_SIZE=\"`./bin/$OS/line -M ${MB}M`\" >> $C

    echo MHZ=\"`./bin/$OS/mhz`\" >> $C
    echo MOTHERBOARD="" >> $C
    echo NETWORKS="" >> $C
    echo OS=`./scripts/os` >> $C
    echo PROCESSORS=`grep processor /proc/cpuinfo | wc -l` >> $C
    echo REMOTE="" >> $C
    echo SLOWFS="YES" >> $C
    echo SYNC_MAX="$nr_task" >> $C
    echo LMBENCH_SCHED="DEFAULT" >> $C
    echo TIMING_O=`./bin/$OS/timing_o` >> $C
    echo RSH="rsh" >> $C
    echo RCP="rcp" >> $C
    echo VERSION=`./scripts/version` >> $C

    [ -n $testcase ] && eval $testcase="YES"
    echo BENCHMARK_HARDWARE=${hardware:-NO} >> $C
    echo BENCHMARK_OS=${os:-NO} >> $C
    echo BENCHMARK_SYSCALL=${syscall:-NO} >> $C
    echo BENCHMARK_SELECT=${select:-NO} >> $C
    echo BENCHMARK_PROC=${proc:-NO} >> $C
    echo BENCHMARK_CTX=${ctx:-NO} >> $C
    echo BENCHMARK_PAGEFAULT=${pagefault:-NO} >> $C
    echo BENCHMARK_FILE=${file:-NO} >> $C
    echo BENCHMARK_MMAP=${mmap:-NO} >> $C
    echo BENCHMARK_PIPE=${pipe:-NO} >> $C
    echo BENCHMARK_UNIX=${unix:-NO} >> $C
    echo BENCHMARK_UDP=${udp:-NO} >> $C
    echo BENCHMARK_TCP=${tcp:-NO} >> $C
    echo BENCHMARK_CONNECT=${connect:-NO} >> $C
    echo BENCHMARK_RPC=${rpc:-NO} >> $C
    echo BENCHMARK_HTTP=${http:-NO} >> $C
    echo BENCHMARK_BCOPY=${bcopy:-NO} >> $C
    echo BENCHMARK_MEM=${mem:-NO} >> $C
    echo BENCHMARK_OPS=${ops:-NO} >> $C

    cd ${bindir} || exit 1
    #for pagefault test
    if ! grep -q '[ -f $FILE ] || dd if=/dev/zero of=$FILE count=1 bs=1G' ./lmbench
    then
        sed -i '/lat_pagefault -P $SYNC_MAX $FILE/i\
            [ -f $FILE ] || dd if=/dev/zero of=$FILE count=1 bs=1G' ./lmbench
    fi

    if [[ $nr_task -eq 1 ]];then
        taskset -c 1 ./lmbench $C
    else
       ./lmbench $C
    fi
}
parse() {
    $TONE_BM_SUITE_DIR/parse.awk
}
