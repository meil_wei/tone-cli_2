# anck-pack-and-boot
## Description
Build rpm package for Anolis Cloud Kernel, then install and boot the new kernel.

Required vars:
 - KERNEL_CI_REPO_URL
 - KERNEL_CI_REPO_BRANCH
 - KERNEL_CI_PR_ID
 - REMOTE_HOST

Optional vars:
 - CK_BUILDER_REPO (default: https://gitee.com/src-anolis-sig/ck-build.git)
 - CK_BUILDER_BRANCH (default: an8-4.19)
 - BUILD_EXTRA (default: base)
 - REBOOT_TIMEOUT (default: 600)

## Homepage
N/A

## Version
1.0

## Category
Functional

## Parameters
N/A

## Results
```
anck_rpm_build: Pass
anck_boot_test: Pass
...
```
