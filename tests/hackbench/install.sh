GIT_URL="https://git.kernel.org/pub/scm/utils/rt-tests/rt-tests.git"
BRANCH="v2.2"
DEP_PKG_LIST="numactl-devel"

build()
{
    make
}

install()
{
    make install prefix="$TONE_BM_RUN_DIR"
}
