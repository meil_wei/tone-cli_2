#!/bin/bash

run()
{
	cd "$TONE_BM_RUN_DIR" ||exit

	numa_node_total_num=$(lscpu  |grep "^NUMA node(s):"| awk '{print $3}')
	socket_num=$(lscpu | grep "^Socket(s)" | awk '{print $2}')

	l3_cache_kb_percpu=$(getconf LEVEL3_CACHE_SIZE | awk '{printf"%d\n", $1/1024}')
	l3_cache_bytes_all=$(($l3_cache_kb_percpu * $socket_num * 1024))
	mem_total_bytes=$(echo "$(grep MemTotal: /proc/meminfo |awk '{print $2}') * 1024" | bc)

	#default 800M
	STREAM_ARRAY_SIZE=100000000
	if [ "$STREAM_ARRAY_SIZE" -gt "$((mem_total_bytes / 4 / 8))" ]; then
		STREAM_ARRAY_SIZE="$(($mem_total_bytes / 4 / 8))"
	fi
	if [ "$STREAM_ARRAY_SIZE" -lt "$((l3_cache_bytes_all / 8 * 4))" ]; then
		echo "Warning: STREAM_ARRAY_SIZE is less than (l3_cache*4)"
	fi
	
	if [ "$nr_task" -ne 1 ];then
		export OMP_NUM_THREADS="$nr_task"
		echo "OMP_NUM_THREADS=$OMP_NUM_THREADS"
	  	logger gcc -O stream.c -o stream -D"STREAM_ARRAY_SIZE=$STREAM_ARRAY_SIZE" -DNTIMES=100 -fopenmp
	else
	  	logger gcc -O stream.c -o stream -D"STREAM_ARRAY_SIZE=$STREAM_ARRAY_SIZE" -DNTIMES=100
	fi
	
	[ -n "$crossnode" ] || crossnode=na
	case $crossnode in
	na)
		logger ./stream
		;;
	cross_0_1)
		if [ "$numa_node_total_num" -le 1 ];then
			echo "Error: The number of numa nodes cannot be less than 1"
			exit 1
		fi
		logger numactl -N 0 -m 1 ./stream
		;;
	custom)
		if [ "$NUMACTL" = '' ];then
			echo "Error: The variable NUMACTL is null, you need to set numactl operating parameters"
			exit 1
		fi
		logger $NUMACTL ./stream
		;;
	esac
}

parse()
{
    "$TONE_BM_SUITE_DIR"/parse.awk
}
