#!/bin/bash
GIT_URL="https://github.com/jeffhammond/STREAM.git"
DEP_PKG_LIST="gcc gcc-gfortran"

build()
{
    make
}

install()
{
    cp -af * "$TONE_BM_RUN_DIR"
}
