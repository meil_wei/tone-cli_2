#WEB_URL="https://github.com/kdlucas/byte-unixbench/archive/refs/tags/v5.1.3.tar.gz"
vcpunum=$(cat /proc/cpuinfo |grep processor | wc -l)
if echo "ubuntu debian uos kylin" | grep $TONE_OS_DISTRO; then
	DEP_PKG_LIST=""
else
	DEP_PKG_LIST="perl-Time-HiRes"
fi

build()
{
    #patch_src unixbench.patch
    	cp $TONE_BM_SUITE_DIR/v5.1.3.tar.gz  $TONE_BM_BUILD_DIR/
    	cd $TONE_BM_BUILD_DIR/
	tar -zxvf v5.1.3.tar.gz
	cd byte-unixbench-5.1.3/UnixBench
	sed -i "s/\"System Benchmarks\", 'maxCopies' => 16/\"System Benchmarks\", 'maxCopies' => $vcpunum/g" Run
	sed -i "s/\"Non-Index Benchmarks\", 'maxCopies' => 16/\"Non-Index Benchmarks\", 'maxCopies' => $vcpunum/g" Run
    
	make all
}

install()
{
    cp -rf * $TONE_BM_RUN_DIR/
}
