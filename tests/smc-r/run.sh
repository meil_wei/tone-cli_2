#!/bin/bash
. $TONE_ROOT/lib/common.sh
. $TONE_ROOT/lib/testinfo.sh

setup()
{
    [ -n "$SERVER" ] && server=${SERVER%% *} || server=127.0.0.1
    [ -n "$CLIENT" ] && client=${CLIENT%% *} || client=127.0.0.1
    echo "Run SMC-R Server on host: $server"

    if [ "x$rdma_type" == "x" ]; then
	    rdma_type="erdma"
    fi

    server_cmd="smcr_test/run.sh -a $client -i $server -r server -e $rdma_type"
    if [ $server = 127.0.0.1 ]; then
        $server_cmd
    else
        ssh $server "TONE_ROOT=$TONE_ROOT TONE_BM_RUN_DIR=$TONE_BM_RUN_DIR server=$server  $TONE_BM_RUN_DIR/$server_cmd"
    fi
}

run() {
	echo "Run smcr test"

	if [ "x$rdma_type" == "x" ]; then
		rdma_type="erdma"
	fi

	client_cmd="smcr_test/run.sh -a $server -i $client -r client -e $rdma_type -c $category -x $case"
	logger $client_cmd
	cp -r smcr_test/logs $TONE_CURRENT_RESULT_DIR
}

parse()
{
    awk -f $TONE_BM_SUITE_DIR/parse.awk
}
