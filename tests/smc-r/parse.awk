#!/usr/bin/awk -f

/skip/ {
        printf("%s Skip\n",$1)
        next
}


/fail/ {
        printf("%s Fail\n",$1)
        next
}

/pass/ {
        printf("%s Pass\n",$1)
}