#!/bin/bash -x

. $TONE_ROOT/lib/disk.sh

add_user()
{
    [ -n "$1" ] || return
    grep -q -w "$1" /etc/passwd && return
    useradd $1
}

disk_type_options()
{
    case $disk_type in
    nfsv3)
        nfsvers=3
        def_mount="-o vers=$nfsvers"
        export TEST_FS_MOUNT_OPTS="-o vers=$nfsvers"
        export MOUNT_OPTIONS="-o vers=$nfsvers"
        fs="nfs"
        ;;
    nfsv4*)
        nfsvers=${fs#nfsv}
        def_mount="-o vers=$nfsvers"
        export TEST_FS_MOUNT_OPTS="-o vers=$nfsvers"
        export MOUNT_OPTIONS="-o vers=$nfsvers"
        fs="nfs"
    esac
}
setup()
{
    add_user "fsgqa"
    add_user "123456-fsgqa"
    add_user "fsgqa2"

    nr_disk=2
    [ -z "$fs2" ] && [ -z "$mkfsopt" ] && nr_disk=4
    
    # set disk
    [ -z "$fs2" ] && fs2="disk"
    if [ "$fs2" == overlay ]; then
        overlay=$fs2
        fs2="disk"
        export mkfsopt=""
    elif [ "$mkfsopt" == bigalloc ]; then
        export mkfsopt="-O bigalloc -C 16k"
    fi
    if [[ "$fs" == ext* ]]; then
        export mkfsopt="$mkfsopt -q -F"
    fi
    echo "mkfsopt is $mkfsopt"
    setup_disk_fs "$fs2" "$nr_disk" "mounted"
    disk_list=($(eval get_dev_partition))
    mounted_list=($(eval get_mount_points))
    
    disk_type_options
    if [[ "$fs" == nfs ]]; then
        umount_nfs
    else
        umount_fs
    fi

    export TEST_DEV="${disk_list[0]}"
    export SCRATCH_DEV="${disk_list[1]}"
    export TEST_DIR=${mounted_list[0]}
    export SCRATCH_MNT=${mounted_list[1]}
    export FSTYP=$fs
}

run()
{   
    [ -z "$test" ] && test="auto"

    # set test options and expunged list
	local opts=""

    [ "$overlay" = overlay ] && opts="-overlay"

    local kver=$(uname -r | cut -d. -f1-2)
    [ -d "blacklists" ] || mkdir blacklists 
    [ -f "blacklists/exclude" ] && rm -f blacklists/exclude
    [ -f $TONE_BM_SUITE_DIR/blacklists/${fs}-exclude ] && \
        cat $TONE_BM_SUITE_DIR/blacklists/${fs}-exclude > blacklists/exclude
    [ -f $TONE_BM_SUITE_DIR/blacklists/${fs}-exclude-${kver} ] && \
        cat $TONE_BM_SUITE_DIR/blacklists/${fs}-exclude-${kver} >> blacklists/exclude
    [ -f "blacklists/exclude" ] && \
        opts="$opts -E blacklists/exclude"
    echo "options is $opts"
    ./check $opts -g $test
}

parse() 
{
    $TONE_BM_SUITE_DIR/parse.awk
}
    
