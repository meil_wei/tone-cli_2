
. $TONE_ROOT/lib/disk.sh

FIO_GIT_URL="https://git.kernel.org/pub/scm/linux/kernel/git/axboe/fio.git"
XFSTESTS_GIT_URL="https://git.kernel.org/pub/scm/fs/xfs/xfstests-dev.git"

DEP_PKG_LIST="acl attr gawk bc  dump e2fsprogs gawk gcc yum-utils
	libtool lvm2 make psmisc quota sed xfsdump  libacl-devel libattr-devel dbench indent
	libaio-devel libuuid-devel python36 sqlite libblkid-devel fio xfsprogs xfsprogs-devel"
DEP_PKG_LIST_BAK="btrfs-progs-devel liburing-devel"
FIO_BRANCH="fio-3.19"

if [ -n "$BRANCH" ]; then
    branch="$BRANCH"
else
	kver=$(uname -r | cut -d. -f1-2)
	branch="master"
	[[ $kver =~ 5\.[0-9]+  ]] && branch="kernel-5.10"
fi

fetch()
{
	echo "\$XFSTESTS_GIT_URL for git clone or update"
	git_clone_mirror $XFSTESTS_GIT_URL $TONE_BM_CACHE_DIR/xfstests-dev
	echo "\$FIO_GIT_URL  for git clone or update"
	git_clone_mirror $FIO_GIT_URL $TONE_BM_CACHE_DIR/fio
}

extract_src()
{
	set_git_clone

	echo "Clone xfstests code to build path"
	mkdir $TONE_BM_BUILD_DIR/xfstests-dev
	cd "$TONE_BM_BUILD_DIR/xfstests-dev" || exit 1
	git_clone_exec "$TONE_BM_CACHE_DIR/xfstests-dev" "$TONE_BM_BUILD_DIR/xfstests-dev" --branch "$branch"

	# build fio from source code and overwrite system version
	fio_ver=$(fio --version 2>/dev/null | sed 's/fio-//g' | cut -d. -f1)
	if [ -z "$fio_ver" ] || [ "$fio_ver" -lt 3 ]; then
		echo "Clone fio code to build path"
		mkdir $TONE_BM_BUILD_DIR/fio
		cd "$TONE_BM_BUILD_DIR/fio" || exit 1
		git_clone_exec "$TONE_BM_CACHE_DIR/fio" "$TONE_BM_BUILD_DIR/fio" --branch "$branch"
	fi
}

build_xfstests(){
	cd $TONE_BM_BUILD_DIR/xfstests-dev
	make configure || return
	./configure --prefix=$TONE_RUN_DIR/
	make && make install
}

fio_build_install()
{
	[ -d "$TONE_BM_BUILD_DIR/fio" ] || return
	# remove system version fio
	rpm -qa | grep -q fio && rpm -e fio
	# build fio from source code
	cd $TONE_BM_BUILD_DIR/fio
	./configure
	make
	make install
}

build()
{
	export CFLAGS="-fcommon"
	fio_build_install
	build_xfstests || return
}

install()
{
    :
}
    
