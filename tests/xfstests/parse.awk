#!/usr/bin/awk -f

/\[expunged\]/ {
        printf("%s: Skip\n",$1)
        next
}

/\[not run\]/ {
        printf("%s: Conf\n",$1)
        next
}


$NF ~ /^[0-9]+s/{
        printf("%s: Pass\n",$1)
        next
}

/output mismatch/{
        printf("%s: Failed\n",$1)
}
