#!/bin/bash
. $TONE_ROOT/lib/common.sh
. $TONE_ROOT/lib/testinfo.sh

run() {
	logger $TONE_BM_RUN_DIR/bin/sysbench $testname --threads=${nr_task:-1} run
}

parse() {
	$TONE_BM_SUITE_DIR/sysbench.awk
}
