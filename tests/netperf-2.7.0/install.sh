#WEB_URL="https://github.com/HewlettPackard/netperf/archive/refs/tags/netperf-2.7.0.tar.gz"

if echo "ubuntu debian uos kylin" | grep $TONE_OS_DISTRO; then
	DEP_PKG_LIST="automake gcc texinfo"
else
	DEP_PKG_LIST="lksctp-tools-devel automake gcc texinfo"
fi

build()
{
    cp $TONE_BM_SUITE_DIR/netperf-2.7.0.tar.gz  $TONE_BM_BUILD_DIR/
    cd $TONE_BM_BUILD_DIR/
    tar -xvf netperf-2.7.0.tar.gz
    cd netperf-netperf-2.7.0
	local configure_flags=(
				--prefix="$TONE_BM_RUN_DIR"
    )
	# Fix configure issue on aarch64 platform:
	# configure: error: cannot guess build type; you must specify one
	if [ $(arch) == "aarch64" ]; then
		configure_flags+=('--build=aarch64-unknown-linux-gnu')
	fi

	# Fix build fail with gcc 10
	#export CFLAGS="-fno-strict-aliasing -fcommon"

	sh autogen.sh
	./configure "${configure_flags[@]}"
	make
}

install()
{
	make install-exec
}
