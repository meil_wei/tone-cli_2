#!/usr/bin/awk -f

$6 && $0 ~ /^[ \t0-9.]+$/ {
	printf("Throughput_tps: %s\n",$6)
}
