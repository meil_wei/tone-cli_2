#!/bin/bash

export PATH="$TONE_BM_RUN_DIR"/bin:$PATH

opt_ip=
[ "$IP" = 'ipv4' ] && opt_ip='-4'
[ "$IP" = 'ipv6' ] && opt_ip='-6'

tst_cmd="netserver $opt_ip"

# SXH: kill netserver if exists.
ns=$(ps -ef | pgrep "netserver -" | grep -v grep)
if (( $? == 0 )); then
    pid=$(echo "$ns" | awk '{print $2}')
    kill -9 "$pid"
    sleep 3
fi  

# set cpu affinity for netserver on multi-processor machine
if [ "$server" = localhost ] && [ "$(nproc)" -gt 1 ]; then
	# bind the netserver to last one of cpu_online_tpy list,
	# bind the netperf to first one of cpu_online_tpy list,
	# so netserver and netperf could run on different sockets
	# or different cores of same cpu -- it depends on the cpu
	# topology of SUT.

	source "$TONE_ROOT"/lib/cpu_affinity.sh
	check_oneline_cpu
	if [ -n "$cpu_online_tpy" ]; then
		mycpu=$(echo "$cpu_online_tpy" | awk '{print $NF}')
		tst_cmd="taskset -c $(echo "$mycpu" | cut -d- -f3) $tst_cmd"
		echo "run netserver on cpu: $(echo "$mycpu" | cut -d- -f3),"\
			"socket: $(echo "$mycpu" | cut -d- -f1),"\
			"core_id: $(echo "$mycpu" | cut -d- -f2)"
	fi
fi

# start netserver
echo "$(date "+%F %T") $tst_cmd"
$tst_cmd
