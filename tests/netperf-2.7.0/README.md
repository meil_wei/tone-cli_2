# Netperf_bclinux

## Description
Netperf is a benchmark that can be use to measure various aspect of networking
performance. The primary foci are bulk (aka unidirectional) data transfer and
request/response performance using either TCP or UDP and the Berkeley Sockets
interface.

## Homepage
https://github.com/HewlettPackard/netperf.git

## Version
last commit 3bc455b

## Category
performance

## Parameters
- test: testcase name
- IP: test protocol

## Results
```
Throughput_tps: 96428.93

```

## Manual Run
```
netperf -4 -t TCP_STREAM -H 127.0.0.1

```
