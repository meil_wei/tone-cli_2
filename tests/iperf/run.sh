#!/bin/bash
. "$TONE_ROOT/lib/cpu_affinity.sh"
server_cmd="iperf3 --server --daemon"
readonly essh="ssh -o StrictHostKeyChecking=no"

setup()
{

    export PATH="$TONE_BM_RUN_DIR"/bin:$PATH
    teardown
    logfile=$(mktemp /tmp/iperf-server.log.XXXXXX)
    [ "$protocol" = "udp" ] && opt_udp=-u
    [ -n "$SERVER" ] && server=${SERVER%% *} || server="127.0.0.1"
    if [ "$server" = "127.0.0.1" ];then
        $server_cmd &> "$logfile"
        [ -n "$cpu_affinity" ] && set_cpu_affinity_server &>/dev/null
    else
        $essh "$server" "$TONE_BM_RUN_DIR/bin/$server_cmd"
    fi
    
}

run()
{
    client_cmd="iperf3 -t $runtime -f M -J -c $server $opt_udp"
    # set cpu affinity if var cpu_affinity is set
    [ -n "$cpu_affinity" ] && set_cpu_affinity_client &>/dev/null
    ${client_cmd} | tee 2>&1 "$TONE_CURRENT_RESULT_DIR"/${testsuite}.json
}

parse()
{
    "$TONE_BM_SUITE_DIR"/parse.py
}

teardown()
{
    pkill iperf3 &>/dev/null
    rm -rf /tmp/iperf-server.log.*
    sleep 2
}
