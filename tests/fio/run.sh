. $TONE_ROOT/lib/disk.sh

init_disk()
{
    nr_disk=1
    if [[ $disk_type == brd ]]; then
        disk_size=$(size_cn_bytes $test_size)
        # enlarge disk to 110% to avoid no enough disk issue: ENOSPC error
        disk_size=$((disk_size * 11 / 10))
        disk_size=$((disk_size >> 10))
    fi
    setup_disk_fs "$disk_type" "$nr_disk" "mounted"
}

init_task()
{
    fsdir=$(get_mount_points)
    echo "[info]: mount point of test partition: $fsdir"
    [ -z "$fsdir" ] && fsdir="/"
    local avail_disk_sz=$(df -B M $fsdir | awk '{print $4}' | tail -n 1)
    # limit test_size under available disk size
    [ $(size_cn_bytes $test_size) -gt $(size_cn_bytes $avail_disk_sz) ] && {
        echo "[Warning]: test_size too large, set it to available disk size instead"
        test_size=$avail_disk_sz
    }

    local must_ops="--test_size $test_size --device $disk_type"
    local opt_ops=""
    [ -z "$nr_task" ] || opt_ops="$opt_ops --nr_task $nr_task"
    [ -z "$bs" ] || opt_ops="$opt_ops --bs $bs"
    [ -z "$rw" ] || opt_ops="$opt_ops --rw $rw"
    [ -z "$ioengine" ] || opt_ops="$opt_ops --ioengine $ioengine"
    [ -z "$direct" ] || opt_ops="$opt_ops --direct $direct"
    opt_ops="$opt_ops --runtime 120"

    logger $TONE_ROOT/bin/fio/gentask.py $must_ops $opt_ops ">" "$TONE_CURRENT_RESULT_DIR/fio.task"
}

setup()
{
    init_disk
    init_task || exit 1
}

run()
{
    local task_path=$TONE_CURRENT_RESULT_DIR/fio.task
    local task_output=$TONE_CURRENT_RESULT_DIR/fio.output
    export PATH="$PATH:$TONE_BM_RUN_DIR/bin"

    if [ -r "$task_path" ]; then
        if [ -n "$cpu_affinity" ]; then
            logger taskset -c 0 fio --output-format=json "$task_path" --output="$task_output"
        else
            logger fio --output-format=json "$task_path" --output="$task_output"
        fi
    else
        echo "Error: $task_path does not exists or nor readable"
    fi
}

teardown()
{
    umount_fs
    local data_file=$(cat $TONE_CURRENT_RESULT_DIR/fio.task | grep filename | awk -F'=' '{print $NF}')
    [ -f $TONE_BM_RUN_DIR/$data_file ] && { echo "cleaning fio data file on root partition..."; rm -f $TONE_BM_RUN_DIR/$data_file ; }
}

parse()
{
    python $TONE_BM_SUITE_DIR/parse.py
}
