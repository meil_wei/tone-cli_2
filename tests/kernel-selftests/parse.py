#!/bin/env python3
import sys
import re

def futex_parse(stdin):
    for line in stdin:
        if re.match(r'(# futex_|# futex.)',line):
            subtest=line.split()[1][:-1]
        if re.match(r'(.*Arguments:)',line):
            subtest = subtest + '.' + line.split(": ")[1].replace(" ", ".").strip()
        if re.match(r'(ok )', line):
            print('%s.%s: %s' % ('futex', subtest, 'Pass'))
        if re.match(r'(not ok )', line):
            print('%s.%s: %s' % ('futex', subtest, 'Fail'))
        if re.match(r'(TAP version 13)', line):
            return

with sys.stdin as stdin:
    for line in stdin:
        if re.match(r'(Running tests in)', line):
            testname = line.split()[-1]
            if ( "futex" == testname.strip() ):
                futex_parse(stdin)
        if re.match(r'(.*ok |.*not ok ).*selftests.*',line):
            subtest = line.split()[-2]
            status = line.split()[-1][1:-1]
            print('%s.%s: %s' % (testname, subtest, status))
        if re.match(r'(ignored_by_tone )',line):
            print('%s: %s' % (line.split()[1], status))
        
            

