#!/bin/bash

mode=${mode:-"base"}

setup()
{
    # prepare
    TEST_JVM_OPTIONS=""
    memory=$(dmidecode -t memory | grep Size: | grep -v 'No Module Installed\|Volatile\|Logical\|Cache'|awk '{sum += $2};END {if ( "MB"==$NF ) print sum/1024;else if ( "GB"==$NF ) print sum}')
    if [[ $memory  == "16" ]]; then
        TEST_JVM_OPTIONS="${TEST_JVM_OPTIONS} -Xms12g -Xmx12g -Xmn10g"
    elif [[ $memory == "32" ]]; then
        TEST_JVM_OPTIONS="${TEST_JVM_OPTIONS} -Xms25g -Xmx25g -Xmn20g"
    elif [[ $memory == "64" ]]; then
        TEST_JVM_OPTIONS="${TEST_JVM_OPTIONS} -Xms50g -Xmx50g -Xmn40g"
    elif [[ $memory == "128" ]]; then
        TEST_JVM_OPTIONS="${TEST_JVM_OPTIONS} -Xms100g -Xmx100g -Xmn90g"
    elif [[ $memory == "256" ]]; then
        TEST_JVM_OPTIONS="${TEST_JVM_OPTIONS} -Xms200g -Xmx200g -Xmn190g"
    elif [[ $memory == "384" ]]; then
        TEST_JVM_OPTIONS="${TEST_JVM_OPTIONS} -Xms300g -Xmx300g -Xmn290g"
    elif [[ $memory == "512" ]]; then
	TEST_JVM_OPTIONS="${TEST_JVM_OPTIONS} -Xms400g -Xmx400g -Xmn390g"
    else
        xms=$(($memory * 75 / 100))
        xmn=$(($xms * 75 / 100))
        TEST_JVM_OPTIONS="${TEST_JVM_OPTIONS} -Xms'$xms'g -Xmx'$xms'g -Xmn'$xmn'g"
    fi
    
    [[ "$mode" = "base" ]] && TEST_JVM_OPTIONS=""


    JAVA_HOME=$(dirname $(dirname $(readlink $(readlink $(which javac)))))
    export JAVA_HOME

    # clean up before test
    teardown
}

run()
{
    #start to test
    benchmarks='startup.helloworld startup.compiler.compiler startup.compress startup.crypto.aes 
    startup.crypto.rsa startup.crypto.signverify startup.mpegaudio startup.scimark.fft startup.scimark.lu 
    startup.scimark.monte_carlo startup.scimark.sor startup.scimark.sparse startup.serial startup.sunflow 
    startup.xml.transform startup.xml.validation compiler.compiler compress crypto.aes crypto.rsa 
    crypto.signverify derby mpegaudio scimark.fft.large scimark.lu.large scimark.sor.large 
    scimark.sparse.large scimark.fft.small scimark.lu.small scimark.sor.small scimark.sparse.small 
    scimark.monte_carlo serial xml.transform xml.validation'
    cd /SPECjvm2008
    ./run-specjvm.sh $benchmarks
}

parse()
{
    "$TONE_BM_SUITE_DIR"/parse.awk
}

teardown()
{
    ps -ef|grep -w "SPECjvm2008.jar"|awk '{print $1}'|xargs -I {} kill -9 {} > /dev/null 2>&1
}

