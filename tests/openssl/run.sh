#!/bin/bash
nr_task=${nr_task:-1}
type=${type:-"rsa4096"}
duration=${duration:-30}

run()
{
    export LD_LIBRARY_PATH=.:$LD_LIBRARY_PATH
    logger openssl speed -multi $nr_task -seconds $duration $type
}

parse()
{
    if [[ $type == "rsa4096" ]]; then
        grep -A 1 'sign    verify    sign/s verify/s'| awk 'END {printf("rsa4096_sign: %s sign/s\n",$(NF-1));printf("rsa4096_verify: %s verify/s\n",$NF)}'
    elif [[ $type == "sha256" ]]; then
        grep 'sha256  ' | awk 'END {printf("sha256_16bytes: %s kbyte/s\n",substr($2,0,(length($2)-1)));\
        printf("sha256_64bytes: %s kbyte/s\n",substr($3,0,(length($3)-1)));\
        printf("sha256_256bytes: %s kbyte/s\n",substr($4,0,(length($4)-1)));\
        printf("sha256_1024bytes: %s kbyte/s\n",substr($5,0,(length($5)-1)));\
        printf("sha256_8192bytes: %s kbyte/s\n",substr($6,0,(length($6)-1)));\
        printf("sha256_16384bytes: %s kbyte/s\n",substr($7,0,(length($7)-1)));}'
    else
        echo "Unsupported type" && return 1
    fi
}
