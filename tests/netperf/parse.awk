#!/usr/bin/awk -f

/^Size.*Okay.*Throughput.*Demand$/ {
    unit = "Mbps1"
    next
}

/^Size.*Throughput.*remote$/ {
    unit = "Mbps2"
    next
}

/^bytes.*secs.\s+per sec/ {
    unit = "tps"
    next
}

$8 && $0 ~ /^[ \t0-9.]+$/ {
    if ( unit == "Mbps1" ) {
        printf("Throughput_Mbps: %s\n",$(NF-2))
    }
	else if ( unit == "Mbps2" ) {
        printf("Throughput_Mbps: %s\n",$5)
    }
	else{
		printf("Throughput_%s: %s\n",unit,$6)
	}
}
