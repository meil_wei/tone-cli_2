#Netperf

## Description
Netperf is a benchmark that can be use to measure various aspect of networking
performance. The primary foci are bulk (aka unidirectional) data transfer and
request/response performance using either TCP or UDP and the Berkeley Sockets
interface.

## Homepage

https://hewlettpackard.github.io/netperf/doc/netperf.html#Top

## Version

2.7.0

## Category

performance

## Parameters

- test: testcase name
- IP: test protocol
- runtime: test run time
- send_size: test file size

## Results

- Throughput_tps：Average transaction rate during data transmission
- Throughput_Mbps：Throughput during data transmission 

## Manual Run

```bash
netperf -4 -t TCP_STREAM -c -C -l 900 -H 127.0.0.1 
```