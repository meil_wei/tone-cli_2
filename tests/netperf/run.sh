. $TONE_ROOT/lib/cpu_affinity.sh

setup()
{
    export PATH="$TONE_BM_RUN_DIR"/bin:$PATH

    [ -n "$send_size" ] && test_options="-- -m $send_size"

     opt_ip=
    [ "$IP" = 'ipv4' ] && opt_ip='-4'
    [ "$IP" = 'ipv6' ] && opt_ip='-6'

    [ -n "$SERVER" ] && server=${SERVER%% *} || server=localhost
    echo "Run netserver on host: $server"

    if [ "$server" = localhost ]; then
        source "$TONE_BM_SUITE_DIR"/netserver.sh
    else
        ssh $server "TONE_ROOT=$TONE_ROOT TONE_BM_RUN_DIR=$TONE_BM_RUN_DIR IP=$IP server=$server $TONE_BM_SUITE_DIR/netserver.sh"
    fi
}

run()
{
    base_cmd="netperf $opt_ip -t $test -c -C -l $runtime -H $server $test_options"

    # check online cpus with cpu_affinity set on multi-processor machine
    if [ "$server" = localhost ] && [ "$(nproc)" -gt 1 ]; then
        check_oneline_cpu
        # the last cpu is reservered for netserver if test on single node
        cpu_online_num=$((cpu_online_num - 1))
        cpu_online_tpy=$(echo "$cpu_online_tpy" | awk '{$NF=""; print $0}')
        cpu_x=$((1 % cpu_online_num))
        [ "$cpu_x" -eq 0 ] && cpu_x=$cpu_online_num
        mycpu=$(echo "$cpu_online_tpy" | awk -v n=$cpu_x '{print $n}')
        test_cmd="taskset -c $(echo "$mycpu" | cut -d- -f3) $base_cmd"
        echo "run netperf on cpu: $(echo "$mycpu" | cut -d- -f3),"\
            "socket: $(echo "$mycpu" | cut -d- -f1),"\
            "core_id: $(echo "$mycpu" | cut -d- -f2)"
    else
        test_cmd="$base_cmd"
    fi

    logger $test_cmd &
    logger wait
}

teardown()
{
    pkill netserver
}

parse()
{
    awk -f "$TONE_BM_SUITE_DIR"/parse.awk
}
