#!/usr/bin/awk -f

/^Dhrystone 2 using register variables/ && $NF ~ /[0-9.]+/ {
    score_dhry2reg[dhry2reg_matched++] = $NF
}
/^Double-Precision Whetstone/ && $NF ~ /[0-9.]+/ {
    score_whetstone_double[whetstone_double_matched++] = $NF
}
/^Execl Throughput/ && $NF ~ /[0-9.]+/ {
    score_execl[execl_matched++] = $NF
}
/^File Copy 1024 bufsize 2000 maxblocks/ && $NF ~ /[0-9.]+/ {
    score_fstime[fstime_matched++] = $NF
}
/^File Copy 256 bufsize 500 maxblocks/ && $NF ~ /[0-9.]+/ {
    score_fsbuffer[fsbuffer_matched++] = $NF
}
/^File Copy 4096 bufsize 8000 maxblocks/ && $NF ~ /[0-9.]+/ {
    score_fsdisk[fsdisk_matched++] = $NF
}
/^Pipe Throughput/ && $NF ~ /[0-9.]+/ {
    score_pipe[pipe_matched++] = $NF
}
/^Pipe-based Context Switching/ && $NF ~ /[0-9.]+/ {
    score_context1[context1_matched++] = $NF
}
/^Process Creation/ && $NF ~ /[0-9.]+/ {
    score_spawn[spawn_matched++] = $NF
}
/^Shell Scripts \(1 concurrent\)/ && $NF ~ /[0-9.]+/ {
    score_shell1[shell1_matched++] = $NF
}
/^Shell Scripts \(8 concurrent\)/ && $NF ~ /[0-9.]+/ {
    score_shell8[shell8_matched++] = $NF
}
/^System Call Overhead/ && $NF ~ /[0-9.]+/ {
    score_syscall[syscall_matched++] = $NF
}
/^System Benchmarks Index Score[[:space:]]+[0-9.]+$/ {
    score_total[total_matched++] = $NF
}

END {
    # parse single mode test results and display the metric score
    if (dhry2reg_matched == 1)
        printf("score: %.2f\n", score_dhry2reg[0])
    if (whetstone_double_matched == 1)
        printf("score: %.2f\n", score_whetstone_double[0])
    if (execl_matched == 1)
        printf("score: %.2f\n", score_execl[0])
    if (fstime_matched == 1)
        printf("score: %.2f\n", score_fstime[0])
    if (fsbuffer_matched == 1)
        printf("score: %.2f\n", score_fsbuffer[0])
    if (fsdisk_matched == 1)
        printf("score: %.2f\n", score_fsdisk[0])
    if (pipe_matched == 1)
        printf("score: %.2f\n", score_pipe[0])
    if (context1_matched == 1)
        printf("score: %.2f\n", score_context1[0])
    if (spawn_matched == 1)
        printf("score: %.2f\n", score_spawn[0])
    if (shell1_matched == 1)
        printf("score: %.2f\n", score_shell1[0])
    if (shell8_matched == 1)
        printf("score: %.2f\n", score_shell8[0])
    if (syscall_matched == 1)
        printf("score: %.2f\n", score_syscall[0])

    # parse default mode test results and display each metric score in original order
    if (dhry2reg_matched == 2) {
        printf("score_dhry2reg_one_process: %.2f\n", score_dhry2reg[0])
        printf("score_whetstone-double_one_process: %.2f\n", score_whetstone_double[0])
        printf("score_execl_one_process: %.2f\n", score_execl[0])
        printf("score_fstime_one_process: %.2f\n", score_fstime[0])
        printf("score_fsbuffer_one_process: %.2f\n", score_fsbuffer[0])
        printf("score_fsdisk_one_process: %.2f\n", score_fsdisk[0])
        printf("score_pipe_one_process: %.2f\n", score_pipe[0])
        printf("score_context1_one_process: %.2f\n", score_context1[0])
        printf("score_spawn_one_process: %.2f\n", score_spawn[0])
        printf("score_shell1_one_process: %.2f\n", score_shell1[0])
        printf("score_shell8_one_process: %.2f\n", score_shell8[0])
        printf("score_syscall_one_process: %.2f\n", score_syscall[0])
        printf("score_total_one_process: %.2f\n\n", score_total[0])

        printf("score_dhry2reg_all_processes: %.2f\n", score_dhry2reg[1])
        printf("score_whetstone-double_all_processes: %.2f\n", score_whetstone_double[1])
        printf("score_execl_all_processes: %.2f\n", score_execl[1])
        printf("score_fstime_all_processes: %.2f\n", score_fstime[1])
        printf("score_fsbuffer_all_processes: %.2f\n", score_fsbuffer[1])
        printf("score_fsdisk_all_processes: %.2f\n", score_fsdisk[1])
        printf("score_pipe_all_processes: %.2f\n", score_pipe[1])
        printf("score_context1_all_processes: %.2f\n", score_context1[1])
        printf("score_spawn_all_processes: %.2f\n", score_spawn[1])
        printf("score_shell1_all_processes: %.2f\n", score_shell1[1])
        printf("score_shell8_all_processes: %.2f\n", score_shell8[1])
        printf("score_syscall_all_processes: %.2f\n", score_syscall[1])
        printf("score_total_all_processes: %.2f\n\n", score_total[1])
    }
}