#GIT_URL="https://github.com/kdlucas/byte-unixbench.git"
GIT_URL="https://gitee.com/mirrors_aliyun/byte-unixbench.git"

if echo "ubuntu debian uos kylin" | grep $TONE_OS_DISTRO; then
	DEP_PKG_LIST=""
else
	DEP_PKG_LIST="perl-Time-HiRes"
fi

build()
{
    patch_src unixbench.patch
    cd UnixBench
    if [ $(uname -m) = aarch64 ]; then
        sed -i 's/-march=native -mtune=native//g' Makefile
    fi
    make
}

install()
{
    cp -rf * $TONE_BM_RUN_DIR/
}
