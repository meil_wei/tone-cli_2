#!/usr/bin/env python
# -*- coding: utf-8 -*-
from core.utils.log import logger
from core.modulemanager import ModuleManager


class CMDDispatcher(ModuleManager):
    def __init__(self):
        logger.debug("%s __init__" % self.__class__.__name__)
        super(CMDDispatcher, self).__init__('cli.run')


class SubcommandPreDispatcher(ModuleManager):
    def __init__(self):
        logger.debug("%s __init__" % self.__class__.__name__)
        super(SubcommandPreDispatcher, self).__init__('subcommand.enter')


class SubcommandDispatcher(ModuleManager):
    def __init__(self):
        logger.debug("%s __init__" % self.__class__.__name__)
        super(SubcommandDispatcher, self).__init__('subcommand.run')


class CMDCommonOptionDispatcher(ModuleManager):
    def __init__(self):
        logger.debug("%s __init__" % self.__class__.__name__)
        super(CMDCommonOptionDispatcher, self).__init__('cli.commonopt')


class JobPreDispatcher(ModuleManager):
    def __init__(self):
        logger.debug("%s __init__" % self.__class__.__name__)
        super(JobPreDispatcher, self).__init__('job.enter')


class JobRunDispatcher(ModuleManager):
    def __init__(self):
        logger.debug("%s __init__" % self.__class__.__name__)
        super(JobRunDispatcher, self).__init__('job.run')


class JobProgressDispatcher(ModuleManager):
    def __init__(self):
        logger.debug("%s __init__" % self.__class__.__name__)
        super(JobProgressDispatcher, self).__init__('job.progress')


class JobPostDispatcher(ModuleManager):
    def __init__(self):
        logger.debug("%s __init__" % self.__class__.__name__)
        super(JobPostDispatcher, self).__init__('job.exit')


class TestPreDispatcher(ModuleManager):
    def __init__(self):
        logger.debug("%s __init__" % self.__class__.__name__)
        super(TestPreDispatcher, self).__init__('test.enter')


class TestRunDispatcher(ModuleManager):
    def __init__(self):
        logger.debug("%s __init__" % self.__class__.__name__)
        super(TestRunDispatcher, self).__init__('test.run')


class TestProgressDispatcher(ModuleManager):
    def __init__(self):
        logger.debug("%s __init__" % self.__class__.__name__)
        super(TestProgressDispatcher, self).__init__('test.progress')


class TestPostDispatcher(ModuleManager):
    def __init__(self):
        logger.debug("%s __init__" % self.__class__.__name__)
        super(TestPostDispatcher, self).__init__('test.exit')


class ParamDispatcher(ModuleManager):
    def __init__(self):
        logger.debug("%s __init__" % self.__class__.__name__)
        super(ParamDispatcher, self).__init__('params')
