#!/usr/bin/env python
# -*- coding: utf-8 -*-


import os
import subprocess
import yaml
import time
from core.module_interfaces import TESTMODULE
from core.utils.tests import read_conf
from core.utils.log import logger


class Archive(TESTMODULE):
    def run(self, subflow):
        if subflow.config.subcommand != 'archive':
            return

        print("Archiving .... ")
        archive_dir = (
            'test_result_path',
        )
        try:
            if subflow.config.parser['name']:
                job_name = subflow.config.parser['name']
            else:
                job_name = ""
            if job_name:
                tarball = job_name + ".tar"
            else:
                if not subflow.config.parser['results']:
                    tarball = "test_tool.tar"
                else:
                    tarball = "test_result.tar"

            pack_only = subflow.config.parser["tar"]
            if not pack_only:
                tarball = tarball + ".gz"

            tarball = os.path.join(
                subflow.config.tone_path,
                tarball
            )
            self.remove_file(tarball)

            # prep job.yaml
            all_test_conf = self.get_all_test_conf(subflow.config.test_conf_path)
            job_file = os.path.join(subflow.config.tone_path, "job.yaml")
            job_yaml = self.get_job_yaml(test_result_path=subflow.config.test_result_path, all_test_conf=all_test_conf, name=job_name)
            self.write_to_yaml(job_yaml, job_file=job_file)

            target_dirs = []
            if not subflow.config.parser['results']:
                for d in subflow.config.frame_tool:
                    target = os.path.join(
                        subflow.config.tone_path, d)
                    print(target)
                    target_dirs.append(target)
            for d in archive_dir:
                print(subflow.config[d])
                target_dirs.append(subflow.config[d])
            target_dirs = [i for i in target_dirs if os.path.exists(i)]
            target_dirs.append(job_file)
            self.create_tar(tarball, target_dirs, pack_only=pack_only)
            print(
                "Done\n"
                "Archive file: {}".format(tarball)
            )
        except Exception as e:
            self.remove_file(tarball)
            raise e

    def remove_file(self, filename):
        if os.path.exists(filename) and os.path.isfile(filename):
            os.remove(filename)
        elif os.path.exists(filename):
            raise Exception("The same target {} exists".format(filename))

    # def create_tar(self, tarball, src):
    #     os.chdir(os.path.dirname(src))
    #     cmd = ["/bin/zip"]
    #     if os.path.exists(tarball):
    #         cmd.append("-urq")
    #     else:
    #         cmd.append("-rq")
    #     cmd.append(tarball)
    #     cmd.append(os.path.basename(src))
    #     ignore_path = os.path.join(
    #         os.path.basename(src),
    #         '.git'
    #     )
    #     if os.path.exists(ignore_path):
    #         cmd.append('-x')
    #         cmd.append(os.path.join(ignore_path, '*'))
    #     logger.debug(" ".join(cmd))
    #     subprocess.check_call(cmd)

    def create_tar(self, tarball, src_dirs, pack_only=False):
        if not src_dirs:
            logger.debug("Create tarball skipped")
            return False
        os.chdir(os.path.dirname(src_dirs[0]))
        if not pack_only:
            cmd = ["/bin/tar", "-zcvf", tarball]
        else:
            cmd = ["/bin/tar", "-cf", tarball]
        for src in src_dirs:
            cmd.append(os.path.basename(src))
        logger.debug(" ".join(cmd))
        subprocess.check_call(cmd)

    def is_result_exist(self, test_result_path, test_conf):
        suite, conf = test_conf.split(":")
        if "=" not in conf:
            _result_path = conf
        else:
            _result_path = "-".join([x.split("=")[1] for x in conf.split(",")])
        _path = os.path.join(suite, _result_path)
        return os.path.exists(os.path.join(test_result_path, _path))

    def get_job_yaml(self, test_result_path, all_test_conf, name=""):
        if not name:
            name = self.gen_job_name()
        job = {"name": name, "test_config": []}
        ip = self.get_eth0_ip()
        test_suites = {}
        for test_conf in all_test_conf:
            if self.is_result_exist(test_result_path, test_conf):
                suite, conf = test_conf.split(":")
                if suite not in test_suites:
                    test_suites[suite] = [conf]
                else:
                    test_suites[suite].append(conf)

        for suite in test_suites:
            test_suite = {"test_suite": suite, "cases": []}
            for case in test_suites[suite]:
                test_case = {"test_case": case, "server": {"ip": ip}}
                test_suite["cases"].append(test_case)
            job["test_config"].append(test_suite)
        return job

    def write_to_yaml(self, job, job_file):
        with open(job_file, "w") as f:
            f.write(yaml.dump(job))

    def get_eth0_ip(self):
        try:
            res = os.popen("ip add show dev eth0 | grep inet").read()
            return res.split()[1].split("/")[0]
        except:
            return None

    def gen_job_name(self):
        return "offline_job_" + str(time.strftime("%Y%m%d_%H-%M-%S", time.localtime()))

    def get_all_test_conf(self, conf_path):
        test_conf = []
        if not os.path.exists(conf_path):
            raise Exception("Can NOT get test conf: {} not exist!".format(conf_path))
            return None
        posible_category = [
            'functional',
            'performance',
            'stress',
        ]
        for category in posible_category:
            curr_path = os.path.join(conf_path, category)
            if not os.path.exists(curr_path):
                continue
            configs = os.listdir(curr_path)
            for c in configs:
                if not c.endswith(".conf"):
                    continue
                suite = c.split(".")[0]
                conf_file = os.path.join(curr_path, c)
                value, indvalue, fields = read_conf(conf_file, suite=suite)
                for i in indvalue:
                    if "testconf" in i:
                        test_conf.append(i["testconf"])
        return test_conf
